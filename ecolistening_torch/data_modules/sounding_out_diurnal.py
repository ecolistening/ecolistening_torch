from dataclasses import dataclass
from conduit.data import BinarySample, TernarySample, TrainValTestSplit
from conduit.data.datasets.utils import CdtDataLoader
from torch.utils.data.dataloader import DataLoader
from conduit.data.datamodules.audio.base import CdtAudioDataModule
from conduit.types import Stage
from torch.nn import Identity
from torch.utils.data import Sampler
from typing import (
    Any,
    List,
    Optional,
    Sequence,
)

from ecolistening_torch.datasets import SoundingOutDiurnal

__all__ = ["SoundingOutDiurnalDataModule"]

@dataclass(kw_only=True)
class SoundingOutDiurnalDataModule(CdtAudioDataModule):
    segment_len: float = 60.0
    sample_rate: int = 48_000
    target_attrs: Optional[List[str]] = None
    sampler: Optional[object] = None

    @staticmethod
    def _batch_converter(batch: TernarySample) -> TernarySample:
        return TernarySample(x=batch.x, y=getattr(batch, "y", None), s=batch.s)

    def make_dataloader(
        self,
        ds: SoundingOutDiurnal,
        *,
        batch_size: int,
        shuffle: bool = False,
        drop_last: bool = False,
        batch_sampler: Optional[Sampler[Sequence[int]]] = None,
    ) -> CdtDataLoader[TernarySample]:
        return CdtDataLoader(
            ds,
            batch_size=batch_size if batch_sampler is None else 1,
            shuffle=shuffle,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
            drop_last=drop_last,
            persistent_workers=self.persist_workers,
            batch_sampler=batch_sampler,
            converter=self._batch_converter,
        )

    def prepare_data(self, *args: Any, **kwargs: Any) -> None:
        SoundingOutDiurnal(
            root=self.root,
            sampler=self.sampler,
            segment_len=self.segment_len,
            sample_rate=self.sample_rate,
            download=True,
        )

    @property
    def train_metadata(self):
        self._train_data_base.metadata

    def predict_dataloader(self):
        eval_train_dataloader = self.make_dataloader(batch_size=self.eval_batch_size, ds=self.train_data)
        return [eval_train_dataloader, self.val_dataloader(), self.test_dataloader()]

    @property
    def _default_transform(self) -> Identity:
        return Identity()

    @property
    def _default_train_transforms(self) -> Identity:
        return Identity()

    @property
    def _default_test_transforms(self) -> Identity:
        return Identity()

    def _get_audio_splits(self) -> TrainValTestSplit[SoundingOutDiurnal]:
        data = SoundingOutDiurnal(
            root=self.root,
            transform=None,
            sampler=self.sampler,
            segment_len=self.segment_len,
            sample_rate=self.sample_rate,
            download=False,
        )
        # then derive data splits
        val, test, train = data.random_split(props=(self.val_prop, self.test_prop), seed=self.seed)
        return TrainValTestSplit(train=train, val=val, test=test)
