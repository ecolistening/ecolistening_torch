import attr
from dataclasses import dataclass, field
from conduit.data import TrainValTestSplit, SubgroupSample
from conduit.data.datasets.utils import CdtDataLoader
from conduit.data.datamodules.base import CdtDataModule
from pathlib import Path
from torch.utils.data import Sampler
from typing import (
    Any,
    List,
    Optional,
    Sequence,
    Union,
)

from ecolistening_torch.datasets import SoundingOutChorusVAEEmbeddings

@dataclass(kw_only=True)
class SoundingOutChorusVAEEmbeddingsDataModule(CdtDataModule):
    root: str
    window_length: int = 6
    hop_length: int = 6
    max_timesteps: Optional[int] = None

    @staticmethod
    def _batch_converter(batch: SubgroupSample) -> SubgroupSample:
        return SubgroupSample(x=batch.x, s=batch.s)

    def make_dataloader(
        self,
        ds: SoundingOutChorusVAEEmbeddings,
        *,
        batch_size: int,
        shuffle: bool = False,
        drop_last: bool = False,
        batch_sampler: Optional[Sampler[Sequence[int]]] = None,
    ) -> CdtDataLoader[SubgroupSample]:
        return CdtDataLoader(
            ds,
            batch_size=batch_size if batch_sampler is None else 1,
            shuffle=shuffle,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
            drop_last=drop_last,
            persistent_workers=self.persist_workers,
            batch_sampler=batch_sampler,
            converter=self._batch_converter,
        )

    def predict_dataloader(self):
        eval_train_dataloader = self.make_dataloader(batch_size=self.eval_batch_size, ds=self.train_data)
        return [
            eval_train_dataloader,
            self.val_dataloader(),
            self.test_dataloader(),
        ]

    def _get_splits(self) -> TrainValTestSplit:
        data = SoundingOutChorusVAEEmbeddings(
            root=self.root,
            window_length=self.window_length,
            hop_length=self.hop_length,
            max_timesteps=self.max_timesteps,
        )
        val, test, train = data.random_split(props=(self.val_prop, self.test_prop), seed=self.seed)
        return TrainValTestSplit(train=train, val=val, test=test)
