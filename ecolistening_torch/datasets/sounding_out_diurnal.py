import math
import numpy as np
import os
import pandas as pd
import platform
import re
import torch
import torchaudio

from astral import LocationInfo, Depression
from astral.sun import dawn, dusk, noon, sunrise, sunset
from conduit.data.datasets.audio.base import CdtAudioDataset
from conduit.data.datasets.utils import AudioTform, UrlFileInfo, download_from_url
from conduit.data.structures import TernarySample
from datetime import datetime
from enum import Enum, auto
from pathlib import Path
from pandas import DataFrame
from pandas.api.types import is_categorical_dtype, is_object_dtype
from pytz import timezone
from pykml import parser as kml
from tqdm import tqdm
from torch import Tensor
from typing import (
    Any,
    Callable,
    ClassVar,
    Dict,
    List,
    Optional,
    Tuple,
    Union,
    cast
)
from typing_extensions import TypeAlias

__all__ = ["SoundingOutDiurnal"]

SampleType: TypeAlias = TernarySample

def try_or(fn, default=None):
    try:
        return fn()
    except:
        return default

class SoundingOutDiurnal(CdtAudioDataset[SampleType, Tensor, Tensor]):
    _METADATA_FILENAME: ClassVar[str] = "metadata.parquet"
    _DATA_DIR: ClassVar[str] = "data"
    _MAP_FILES: ClassVar[List] = ["ecuador.kml", "uk.kml"]

    _FILE_REGEX = re.compile(
        r'^(.{2,6})-(\d{2})_(\d{1})_'
        r'(\d{4})(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])_'
        r'([0-5]?\d)([0-5]?\d).*\.wav$'
    )

    _FILE_INFO: List[UrlFileInfo] = [
        UrlFileInfo(
            name="data.zip",
            url="https://zenodo.org/record/<ID>/files/data.zip",
            md5="<INPUT HASH HERE>",
        )
    ]

    _SITE_TO_QUALITY = {
        **{ loc: f"UK{q}" for q, loc in enumerate(["PL", "KN", "BA"]) },
        **{ loc: f"EC{q}" for q, loc in enumerate(["TE", "FS", "PO"]) },
    }

    _SITE_TO_TZ = {
        **{ loc: "Europe/London" for loc in ["PL", "KN", "BA"] },
        **{ loc: "America/Guayaquil" for loc in ["TE", "FS", "PO"] },
    }

    num_frames_in_segment: int
    _MAX_AUDIO_LEN: int = 60
    _BITS_PER_BYTE: int = 8
    _AUDIO_SAMPLE_RATE: int = 48_000
    _BIT_RATE: int = 16

    def __init__(
        self,
        root: str,
        *,
        sampler: Optional[object] = None,
        transform: Optional[AudioTform] = None,
        download: bool = True,
        sample_rate: int = 48_000,
        segment_len: float = 60,
        reset_index: bool = False,
    ) -> None:
        self.base_dir = Path(root).expanduser()
        self.audio_dir = self.base_dir / self._DATA_DIR
        self.download = download
        self.sample_rate = sample_rate
        self.segment_len = segment_len
        self.sampler = sampler

        if self.download:
            self._download_files()
        self._check_files()

        if reset_index or not (self.base_dir / self._METADATA_FILENAME).exists():
            self._extract_metadata()

        # load metadata file
        self.metadata = pd.read_parquet(self.base_dir / self._METADATA_FILENAME)
        self.metadata.index.name = 'file_i'
        metadata = self._clean_metadata(self.metadata)

        self.metadata["file_path"] = self.audio_dir / self.metadata.file_name
        if self.segment_len < self._MAX_AUDIO_LEN:
            self.metadata, audio_dir = self._preprocess_files()
            s = torch.stack([torch.tensor(self.metadata.index), torch.tensor(self.metadata.file_i), torch.tensor(self.metadata.timestep)], dim=0)
        else:
            s = torch.as_tensor(self.metadata.index)

        x = self.metadata.file_path.to_numpy()

        super().__init__(x=x, y=None, s=s, transform=transform, audio_dir=self.audio_dir)

    def load_sample(self, index: int) -> Tensor:
        return self.sampler(self.audio_dir / self.x[index])

    @property
    def segment_len(self) -> float:
        return self._segment_len

    @segment_len.setter
    def segment_len(self, value: float) -> None:
        if value <= 0: raise ValueError("segment length must be positive")
        self._segment_len = min(value, self._MAX_AUDIO_LEN)
        self.num_frames_in_segment = int(self.segment_len * self.sample_rate) 
    @property
    def sampler(self) -> Callable:
        return self._sampler

    @sampler.setter
    def sampler(self, sampler: Callable) -> None:
        self._sampler = self._default_sampler if sampler is None else sampler

    @staticmethod
    def _label_encode(
        data: Union[pd.DataFrame, pd.Series],
        inplace=True
    ) -> Union[pd.DataFrame, pd.Series]:
        """label encode the extracted concept/context/superclass information."""
        data = data.copy(deep=not inplace)
        if isinstance(data, pd.Series):
            if is_object_dtype(data) or is_categorical_dtype(data):
                data.update(data.factorize()[0])
                data = data.astype(np.int64)
        else:
            for col in data.columns:
                # Add a new column containing the label-encoded data
                if is_object_dtype(data[col]) or is_categorical_dtype(data[col]):
                    data[col] = data[col].factorize()[0]
        return data

    def _check_files(self) -> None:
        if not self.audio_dir.exists():
            raise RuntimeError(f"Data not found at location {self.base_dir.resolve()}. Have you downloaded it?")

    def _download_files(self) -> None:
        """download files from remote"""
        # create necessary directories if they don't already exist.
        self.base_dir.mkdir(parents=True, exist_ok=True)
        # download files from remote
        for finfo in self._FILE_INFO:
            download_from_url(
                file_info=finfo,
                root=self.base_dir,
                logger=self.logger,
                remove_finished=True
            )

    def _preprocess_files(self) -> None:
        """chunk audio into segments of specified length (seconds) and persist"""
        segment_dir = self.audio_dir / f"{self.segment_len}s_segments"
        if segment_dir.exists():
            self.logger.info(f"Loading pre-processed audio segments from {str(segment_dir)}")
            return pd.read_parquet(segment_dir / "metadata.parquet"), segment_dir
        segment_dir.mkdir(parents=True, exist_ok=True)
        backend = "soundfile" if platform.system() == "Windows" else "sox"
        waveform_paths = self.audio_dir / self.metadata["file_name"]
        segment_metadata = []
        for file_i, path in tqdm(enumerate(waveform_paths), desc=f"Saving audio segments to {str(segment_dir)}/ ...."):
            waveform_filename = path.stem
            waveform, sr = torchaudio.load(path)
            audio_len = waveform.size(-1) / sr
            frac_remainder, num_segments = math.modf(audio_len / self.segment_len)
            num_segments = int(num_segments)
            num_samples = int(num_segments * ((self.segment_len * sr) - 1))
            waveform_segments = waveform[:, :num_samples].chunk(chunks=num_segments, dim=-1)
            for seg_i, segment in enumerate(waveform_segments):
                file_path = segment_dir / f"{waveform_filename}_{seg_i}.wav"
                torchaudio.save(uri=str(file_path), src=segment, sample_rate=sr, backend=backend)
                segment_metadata.append((file_i, seg_i, waveform_filename, str(file_path)))
        segment_metadata = pd.DataFrame(segment_metadata, columns=["file_i", "timestep", "segment_file_name", "file_path"])
        segment_metadata.to_parquet(str(segment_dir / "metadata.parquet"), index=True, engine="auto")
        self.logger.info(f"Loading pre-processed audio segments from {str(segment_dir)}")
        return segment_metadata, segment_dir

    def _default_sampler(self, file_path: str) -> Tensor:
        """resample sample audio using specified sample rate and load a segment"""
        metadata = torchaudio.info(str(file_path))
        backend = "soundfile" if platform.system() == "Windows" else "sox"
        num_frames_segment = int(self.num_frames_in_segment / self.sample_rate * metadata.sample_rate)
        high = max(1, metadata.num_frames - num_frames_segment)
        frame_offset = torch.randint(low=0, high=high, size=(1,))
        waveform, _ = torchaudio.load(str(file_path), num_frames=num_frames_segment, backend=backend)
        return torchaudio.functional.resample(waveform, orig_freq=metadata.sample_rate, new_freq=self.sample_rate).squeeze()

    def _extract_coordinates(self):
        """extract recorder co-ordinate information from kml files"""
        coordinates = {}
        for map_file in self._MAP_FILES:
            document = kml.parse(open(self.base_dir / 'maps' / map_file, "r"))
            for site in document.getroot().Document.Folder.Placemark:
                site_name = str(site.name).upper()
                coordinates[site_name] = {}
                coords = str(site.Point.coordinates).split(',')
                for metric, coord in zip(['longitude', 'latitude', 'altitude'], coords):
                    coordinates[site_name][metric] = float(coord)
        return coordinates

    def _extract_file_metadata(self, row):
        """extract metadata from filename"""
        fields = self._FILE_REGEX.search(row.file_name)
        site, recorder_no, channel, year, month, day, hours, minutes = fields.groups()
        habitat = self._SITE_TO_QUALITY[site]
        country = habitat[0:2]
        tz = timezone(self._SITE_TO_TZ[site])
        dt = datetime(int(year), int(month), int(day), int(hours), int(minutes))
        timestamp = tz.localize(dt)
        return dict(
            site=site,
            country=country,
            habitat=habitat,
            recorder=f"{site}{recorder_no}",
            recorder_no=recorder_no,
            channel=channel,
            timestamp=timestamp,
        )

    def _extract_astral_metadata(self, row):
        """calculate dawn/dusk times using site co-ordinates"""
        info = LocationInfo(row.site, row.country, row.timestamp, row.latitude, row.longitude)
        return dict(
            time_at_sunrise=sunrise(info.observer, date=row.timestamp.date(), tzinfo=row.timestamp.tzinfo),
            time_at_sunset=sunset(info.observer, date=row.timestamp.date(), tzinfo=row.timestamp.tzinfo),
            time_at_astronomical_dawn=try_or(lambda: dawn(info.observer, depression=Depression.ASTRONOMICAL, date=row.timestamp.date(), tzinfo=row.timestamp.tzinfo)),
            time_at_nautical_dawn=try_or(lambda: dawn(info.observer, depression=Depression.NAUTICAL, date=row.timestamp.date(), tzinfo=row.timestamp.tzinfo)),
            time_at_civil_dawn=try_or(lambda: dawn(info.observer, depression=Depression.CIVIL, date=row.timestamp.date(), tzinfo=row.timestamp.tzinfo)),
            time_at_astronomical_dusk=try_or(lambda: dusk(info.observer, depression=Depression.ASTRONOMICAL, date=row.timestamp.date(), tzinfo=row.timestamp.tzinfo)),
            time_at_nautical_dusk=try_or(lambda: dusk(info.observer, depression=Depression.NAUTICAL, date=row.timestamp.date(), tzinfo=row.timestamp.tzinfo)),
            time_at_civil_dusk=try_or(lambda: dusk(info.observer, depression=Depression.CIVIL, date=row.timestamp.date(), tzinfo=row.timestamp.tzinfo)),
        )

    def _clean_metadata(self, metadata) -> DataFrame:
        """remove files shorter than specified duration and/or if they don't exist on disk"""
        metadata = metadata.loc[metadata.duration_seconds >= self.segment_len]
        return metadata[[(self.audio_dir / file_name).exists() for file_name in metadata.file_name]]

    def _extract_metadata(self) -> None:
        """generate a single master metadata parquet file"""
        self.logger.info("extracting metadata...")
        # load file names
        file_names = [file_name for file_name in os.listdir(self.audio_dir) if file_name.endswith('.wav')]
        metadata = DataFrame(columns=["file_name"], data=file_names)
        # extract metadata from filename and time data using atral
        file_metadata = metadata.apply(self._extract_file_metadata, axis=1, result_type="expand")
        metadata = pd.concat([metadata, file_metadata], axis=1)
        # extract recorder lat/lng/alt from KML files
        coordinates = self._extract_coordinates()
        coord_metadata = metadata.apply(lambda row: coordinates[row.recorder], axis=1, result_type="expand")
        metadata = pd.concat([metadata, coord_metadata], axis=1)
        # extract astral metadata
        time_metadata = metadata.apply(self._extract_astral_metadata, axis=1, result_type="expand")
        metadata = pd.concat([metadata, time_metadata], axis=1)
        # calculate duration using file size given we know the sample and bit rate
        metadata.loc[:, "file_size"] = [os.path.getsize(file_name) for file_name in self.audio_dir / metadata.file_name]
        metadata.loc[:, "duration_seconds"] = metadata.file_size * self._BITS_PER_BYTE // self._AUDIO_SAMPLE_RATE // self._BIT_RATE
        metadata.index.name = 'file_i'
        # persist metadata file
        metadata.to_parquet(
            self.base_dir / self._METADATA_FILENAME,
            engine="auto",
            index=True,
        )
        self.logger.info("...metadata extracted")

    def _default_sampler(self, file_path: str) -> Tensor:
        """resample sample audio using specified sample rate and load a segment"""
        # get metadata first
        metadata = torchaudio.info(str(file_path))
        # compute number of frames to take with the real sample rate
        num_frames_segment = int(
            self.num_frames_in_segment / self.sample_rate * metadata.sample_rate
        )
        # get random sub-sample
        high = max(1, metadata.num_frames - num_frames_segment)
        frame_offset = torch.randint(low=0, high=high, size=(1,))
        # load segment
        backend = "soundfile" if platform.system() == "Windows" else "sox"
        waveform, _ = torchaudio.load(str(file_path), num_frames=num_frames_segment, frame_offset=frame_offset, backend=backend)
        # resample to correct sample rate
        return torchaudio.functional.resample(
            waveform,
            orig_freq=metadata.sample_rate,
            new_freq=self.sample_rate,
        )

if __name__ == '__main__':
    data = SoundingOutDiurnal(
        root="~/data/sounding_out_diurnal",
        sample_rate=48_000,
        segment_len=50,
        reset_index=True,
    )
    assert data[0].x.shape == (1, 48_000 * 50)
