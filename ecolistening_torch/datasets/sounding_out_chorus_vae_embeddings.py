import re
import attr
import einops
import numpy as np
import pandas as pd
import torch

from torch import Tensor
from torch.distributions.normal import Normal
from pathlib import Path
from pandas.api.types import is_categorical_dtype, is_object_dtype
from conduit.data.datasets.base import CdtDataset
from numpy.typing import NDArray
from typing import (
    List,
    Optional,
    Tuple,
    Union,
)

class SoundingOutChorusVAEEmbeddings(CdtDataset):
    _METADATA_FILENAME = "metadata.parquet"

    def __init__(
        self,
        root: str,
        window_length: int = 6,
        hop_length: int = 6,
        max_timesteps: Optional[int] = None,
    ) -> None:
        self.base_dir = Path(root).expanduser()
        hop_features_path = self.base_dir / f"window_length={str(window_length)}_hop_length={str(hop_length)}_features.parquet"
        assert hop_features_path.is_file(), "specified hop length does not exist, have you encoded this file?"
        features = pd.read_parquet(hop_features_path)
        metadata = pd.read_parquet(self.base_dir / self._METADATA_FILENAME)
        metadata.index.name = "i"
        index = metadata.index.get_level_values("i")
        # extract dimensionality of latent features and number of timesteps from the metadata
        self.latent_dim = len(features.columns) // 2
        self.num_timesteps = features.index.get_level_values("t").max() + 1
        # set the specified maximum number of timesteps to use
        self.max_timesteps = max_timesteps or self.num_timesteps
        self.features, self.metadata = features.align(metadata, join='inner', axis=0)
        # unpack flattened features into sequence of features
        x = einops.rearrange(torch.tensor(self.features.to_numpy()), '(n seq) ld -> n seq ld', seq=self.num_timesteps)
        super().__init__(x=x, y=None, s=torch.tensor(index))

    def _sample_x(self, idx, **kwargs) -> Tensor:
        q_z = self.x[idx]
        q_z = q_z[:self.max_timesteps, :]
        return q_z.unsqueeze(0)

    def log_sigma_x_1d_histogram(
        self,
        num_bins: int = 20,
        z_score: float = 1.282
    ) -> Tensor:
        x = self.log_sigma_x.to_numpy().flatten()
        _min = x.mean() - z_score * x.std()
        _max = x.mean() + z_score * x.std()
        x = x[np.nonzero(np.logical_and(x >=_min, x <= _max))]
        hist = np.zeros(num_bins + 1)
        bins = np.linspace(x.min(), x.max(), num_bins + 1)
        for i in range(num_bins):
            hist[i] = ((bins[i] <= x) & (x < bins[i + 1])).sum()
        hist /= len(x)
        return hist, bins

    def log_sigma_x_2d_histogram(
        self,
        num_bins: int = 20,
        z_score: float = 1.282
    ) -> Tensor:
        x = self.log_sigma_x.to_numpy()
        _min = x.mean() - z_score * x.std()
        _max = x.mean() + z_score * x.std()
        x = x[np.nonzero(np.logical_and(x >=_min, x <= _max))]
        hist = np.zeros((num_bins + 1, self.latent_dim))
        bins = np.linspace(x.min(), x.max(), num_bins + 1)
        for i in range(num_bins):
            hist[i, ...] = ((bins[i] < x) & (x < bins[i + 1])).sum(axis=0)
        hist /= len(x)
        return hist, bins

    @property
    def mu_x(self) -> NDArray:
        return self.features.loc[:, self.mu_x_columns]

    @property
    def log_sigma_x(self) -> NDArray:
        return self.features.loc[:, self.log_sigma_x_columns]

    @property
    def sigma_x(self) -> NDArray:
        return np.exp(0.5 * self.log_sigma_x)

    @property
    def mu_x_columns(self) -> Tuple[str]:
        return tuple(f"$\mu^{{{d}}}$" for d in range(self.latent_dim))

    @property
    def log_sigma_x_columns(self) -> Tuple[str]:
        return tuple(f"$\log(\sigma^{{{d}}})$" for d in range(self.latent_dim))
