import numpy as np
import pandas as pd
import platform
import math
import re
import shutil
import torch
import torchaudio
import zipfile

from astral import LocationInfo, Depression
from astral.sun import dawn, dusk, noon, sunrise, sunset
from conduit.data.datasets.audio.base import CdtAudioDataset
from conduit.data.datasets.utils import AudioTform, UrlFileInfo, download_from_url
from conduit.data.structures import TernarySample
from datetime import datetime
from enum import auto
from functools import cached_property
from pandas.api.types import is_categorical_dtype, is_object_dtype
from pandas import DataFrame, Index
from pathlib import Path
from pytz import timezone
from pykml import parser as kml
from tempfile import TemporaryDirectory
from torch import Tensor
from tqdm import tqdm
from typing import (
    Callable,
    ClassVar,
    Dict,
    Final,
    List,
    Optional,
    Tuple,
    Union,
)
from typing_extensions import TypeAlias

__all__ = [
    "SoundingOutChorus",
]

SampleType: TypeAlias = TernarySample

def try_or(fn, default=None):
    try:
        return fn()
    except:
        return default

class SoundingOutChorus(CdtAudioDataset[SampleType, Tensor, Tensor]):
    """Dataset for audio data collected in various geographic locations."""
    _METADATA_FILENAME: ClassVar[str] = "metadata.parquet"
    _DATA_DIR: ClassVar[str] = "data"
    _LABELS_DIR: ClassVar[str] = "labels"
    _MAPS_DIR: ClassVar[str] = "maps"
    _MAP_FILES: ClassVar[List] = ["ecuador.kml", "uk.kml"]

    _EC_CORE_LABELS_FILENAME: ClassVar[str] = "EC_CORE.csv"
    _EC_ACOUSTIC_IDX_FILENAME: ClassVar[str] = "EC_AI.csv"
    _EC_BIRD_LABELS_FILENAME: ClassVar[str] = "EC_BIRD_ID.csv"
    _EC_BIRD_KEY_FILENAME: ClassVar[str] = "EC_BIRD_KEY.csv"
    _EC_OTHER_LABELS_FILENAME: ClassVar[str] = "EC_OTHER.csv"

    _UK_CORE_LABELS_FILENAME: ClassVar[str] = "UK_CORE.csv"
    _UK_ACOUSTIC_IDX_FILENAME: ClassVar[str] = "UK_AI.csv"
    _UK_BIRD_LABELS_FILENAME: ClassVar[str] = "UK_BIRD_ID.csv"
    _UK_BIRD_KEY_FILENAME: ClassVar[str] = "UK_BIRD_KEY.csv"
    _UK_OTHER_LABELS_FILENAME: ClassVar[str] = "UK_OTHER.csv"

    _ACOUSTIC_IDX: ClassVar[str] = "acoustic_indices"
    _BIRD_IDS: ClassVar[str] = "bird_ids"
    _BIRD_PRESENCE: ClassVar[str] = "bird_presence"

    _FILE_REGEX = re.compile(
        r'^(.{2,6})-(\d{2})_(\d{1})_'
        r'(\d{4})(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])_'
        r'([0-5]?\d)([0-5]?\d).*\.wav$'
    )

    _FILE_INFO: List[UrlFileInfo] = [
        UrlFileInfo(
            name="labels.zip",
            url="https://zenodo.org/record/8338003/files/labels.zip",
            md5="6f1d59cf1317ce53948fa9bacede5504",
        ),
        UrlFileInfo(
            name="data.zip",
            url="https://zenodo.org/record/8338003/files/data.zip",
            md5="69e3e7213b65af2bf7e75a053d7cfa04",
        ),
        UrlFileInfo(
            name="maps.zip",
            url="https://zenodo.org/record/8338003/files/maps.zip",
            md5="d24a0585e26b3a138b083cc4ceaa8bc2",
        ),
    ]

    _COUNTRY_TO_TZ = {
        "United Kingdom": "Europe/London",
        "Ecuador": "America/Guayaquil"
    }

    num_frames_in_segment: int
    _MAX_AUDIO_LEN: Final[int] = 60.0
    _BITS_PER_BYTE: int = 8
    _AUDIO_SAMPLE_RATE: int = 48_000
    _BIT_RATE: int = 16

    def __init__(
        self,
        root: str,
        *,
        target_attrs: Optional[List[str]] = None,
        sampler: Optional[object] = None,
        transform: Optional[AudioTform] = None,
        download: bool = True,
        segment_len: float = 60.0,
        sample_rate: int = 48_000,
        reset_index: bool = False,
    ) -> None:
        self.base_dir = Path(root).expanduser()
        self.data_dir = self.base_dir / self._DATA_DIR
        self.labels_dir = self.base_dir / self._LABELS_DIR
        self.maps_dir = self.base_dir / self._MAPS_DIR
        self.download = download
        self.sample_rate = sample_rate
        self.segment_len = segment_len
        self.sampler = sampler
        # fetch from remote
        if self.download:
            self._download_files()
        # check data exists on device
        self._check_files()
        # extract labels from indices files if first time
        if reset_index or not (self.base_dir / self._METADATA_FILENAME).exists():
            self._extract_metadata()
        # load metadata file
        self.metadata = pd.read_parquet(self.base_dir / self._METADATA_FILENAME)
        self.metadata.index.name = "file_i"
        # merge in acoustic indices
        if target_attrs is not None and self._ACOUSTIC_IDX in target_attrs:
            self.metadata = self.metadata.merge(self.acoustic_idx, on="file_name", how="inner")
        # merge in bird ids
        if target_attrs is not None and self._BIRD_IDS in target_attrs:
            target_attrs = list(filter(lambda t: t != self._BIRD_IDS, target_attrs))
            mask = self.bird_ids.columns != 'file_name'
            target_attrs += list(self.bird_ids.columns[mask])
            self.metadata = self.metadata.merge(self.bird_ids, on="file_name", how="inner")
        # merge in bird presence
        if target_attrs is not None and self._BIRD_PRESENCE in target_attrs:
            target_attrs = list(filter(lambda t: t != self._BIRD_PRESENCE, target_attrs))
            mask = self.bird_presence.columns != 'file_name'
            target_attrs += list(self.bird_presence.columns[mask])
            self.metadata = self.metadata.merge(self.bird_presence, on="file_name", how="inner")

        audio_dir = self.data_dir
        self.metadata["file_path"] = audio_dir / self.metadata.file_name
        if self.segment_len < self._MAX_AUDIO_LEN:
            self.metadata, audio_dir = self._preprocess_files()
            s = torch.stack([torch.tensor(self.metadata.index), torch.tensor(self.metadata.file_i), torch.tensor(self.metadata.timestep)], dim=0)
        else:
            s = torch.as_tensor(self.metadata.index)

        # x specifies the files to be loaded by parent dataset class
        x = self.metadata["file_path"].to_numpy()

        # only pass y if available
        if target_attrs is not None:
            y = torch.as_tensor(self._label_encode(self.metadata[target_attrs], inplace=True).to_numpy())
        else:
            y = None

        # add file index as additional tensor
        s = torch.as_tensor(self.metadata.index)

        super().__init__(x=x, y=y, s=s, transform=transform, audio_dir=audio_dir)

    def load_sample(self, index: int) -> Tensor:
        return self.sampler(self.audio_dir / self.x[index])

    @property
    def segment_len(self) -> float:
        return self._segment_len

    @segment_len.setter
    def segment_len(self, value: float) -> None:
        if value <= 0:
            raise ValueError("Segment length must be positive.")
        value = min(value, self._MAX_AUDIO_LEN)
        self._segment_len = value
        self.num_frames_in_segment = int(self.segment_len * self.sample_rate)

    @property
    def sampler(self) -> Callable:
        return self._sampler

    @sampler.setter
    def sampler(self, sampler: Callable) -> None:
        self._sampler = self._default_sampler if sampler is None else sampler

    @cached_property
    def ec_bird_ids(self) -> DataFrame:
        """load ecuador bird ids"""
        return pd.read_csv(self.labels_dir / self._EC_BIRD_LABELS_FILENAME)

    @cached_property
    def uk_bird_ids(self) -> DataFrame:
        """load uk bird ids"""
        return pd.read_csv(self.labels_dir / self._UK_BIRD_LABELS_FILENAME)

    @cached_property
    def bird_ids(self) -> DataFrame:
        """concatenate and cache uk bird ids"""
        # create union of columns
        shared_columns = self.uk_bird_ids.columns.union(self.ec_bird_ids.columns)
        # reindex each df filling NaNs to 0
        return pd.concat([
            self.uk_bird_ids.reindex(columns=shared_columns, fill_value=0),
            self.ec_bird_ids.reindex(columns=shared_columns, fill_value=0),
        ])

    @cached_property
    def bird_keys(self) -> DataFrame:
        """concatenate and cache bird name key"""
        return pd.concat([
            pd.read_csv(self.labels_dir / self._UK_BIRD_KEY_FILENAME),
            pd.read_csv(self.labels_dir / self._EC_BIRD_KEY_FILENAME),
        ], axis=1)

    @cached_property
    def uk_bird_presence(self) -> DataFrame:
        """generate uk bird presence label"""
        birds = self.uk_bird_ids.loc[:, self.uk_bird_ids.columns != 'file_name']
        presence = (birds > 0).astype(int)
        presence.rename(columns={ col: f"{col}_present" for col in presence.columns }, inplace=True)
        return pd.concat([self.uk_bird_ids.file_name, presence], axis=1)

    @cached_property
    def ec_bird_presence(self) -> DataFrame:
        """generate ec bird presence label"""
        birds = self.ec_bird_ids.loc[:, self.ec_bird_ids.columns != 'file_name']
        presence = (birds > 0).astype(int)
        presence.rename(columns={ col: f"{col}_present" for col in presence.columns }, inplace=True)
        return pd.concat([self.ec_bird_ids.file_name, presence], axis=1)

    @cached_property
    def bird_presence(self) -> DataFrame:
        """concate country presence metadata"""
        shared_columns = self.uk_bird_presence.columns.union(self.ec_bird_presence.columns)
        return pd.concat([
            self.uk_bird_presence.reindex(columns=shared_columns, fill_value=0),
            self.ec_bird_presence.reindex(columns=shared_columns, fill_value=0),
        ])

    @cached_property
    def acoustic_idx(self):
        """load and cache acoustic index metadata"""
        ec_ac_idx = pd.read_csv(self.labels_dir / self._EC_ACOUSTIC_IDX_FILENAME)
        uk_ac_idx = pd.read_csv(self.labels_dir / self._UK_ACOUSTIC_IDX_FILENAME)
        return pd.concat([ec_ac_idx, uk_ac_idx])

    @staticmethod
    def _label_encode(
        data: Union[pd.DataFrame, pd.Series],
        inplace=True
    ) -> Union[pd.DataFrame, pd.Series]:
        """label encode the extracted concept/context/superclass information."""
        data = data.copy(deep=not inplace)
        if isinstance(data, pd.Series):
            if is_object_dtype(data) or is_categorical_dtype(data):
                data.update(data.factorize()[0])
                data = data.astype(np.int64)
        else:
            for col in data.columns:
                # add a new column containing the label-encoded data
                if is_object_dtype(data[col]) or is_categorical_dtype(data[col]):
                    data[col] = data[col].factorize()[0]
        return data

    def _download_files(self) -> None:
        """download files from remote"""
        self.base_dir.mkdir(parents=True, exist_ok=True)
        for finfo in self._FILE_INFO:
            download_from_url(file_info=finfo, root=self.base_dir, logger=self.logger, remove_finished=True)

    def _check_files(self) -> None:
        """assert files exist and zip unpacked"""
        if not self.labels_dir.exists():
            if zipfile.is_zipfile(self.labels_dir):
                raise RuntimeError("labels.zip file not unzipped.")
            else:
                raise FileNotFoundError(f"labels files not found at location {self.labels_dir.resolve()}. have you downloaded it?")

        if not self.data_dir.exists():
            if zipfile.is_zipfile(self.data_dir):
                raise RuntimeError(f"data.zip file not unzipped.")
            else:
                raise RuntimeError(f"data not found at location {path.resolve()}. have you downloaded it?")

    def _extract_metadata(self) -> None:
        """unpack uk and ec metadata and merge into a single master parquet file"""
        self.logger.info("extracting metadata...")
        ec_labels = pd.read_csv(self.labels_dir / self._EC_CORE_LABELS_FILENAME, encoding="ISO-8859-1", parse_dates=True, date_parser=pd.to_datetime, sep=',')
        uk_labels = pd.read_csv(self.labels_dir / self._UK_CORE_LABELS_FILENAME, encoding="ISO-8859-1", parse_dates=True, date_parser=pd.to_datetime, sep=',')
        metadata = pd.concat([uk_labels, ec_labels])
        metadata.reset_index(drop=True, inplace=True)
        metadata.index.rename('file_i', inplace=True)
        existing_files = [(self.data_dir / file_name).exists() for file_name in metadata.file_name]
        metadata = metadata[existing_files]
        metadata["recorder_no"] = metadata.recorder.map(lambda label: int(label[2:]))
        recorder_metadata = metadata.apply(
            lambda x: { "recorder_model": "SM2" } if x.recorder_no in list(range(1, 8)) else { "recorder_model": "SM3" },
            axis=1, result_type='expand',
        )
        metadata = pd.concat([metadata, recorder_metadata], axis=1)
        metadata.drop(["timestamp", "hour"], axis=1, inplace=True)
        time_metadata = metadata.apply(self.__extract_time_metadata, axis=1, result_type='expand')
        metadata = pd.concat([metadata, time_metadata], axis=1)
        metadata.to_parquet(self.base_dir / self._METADATA_FILENAME, engine="auto", index=True)
        self.logger.info("...metadata extracted and indexed")

    def _preprocess_files(self) -> None:
        """chunk audio into segments of specified length (seconds) and persist"""
        segment_dir = self.data_dir / f"{self.segment_len}s_segments"
        if segment_dir.exists():
            self.logger.info(f"Loading pre-processed audio segments from {str(segment_dir)}")
            return pd.read_parquet(segment_dir / "metadata.parquet"), segment_dir
        segment_dir.mkdir(parents=True, exist_ok=True)
        backend = "soundfile" if platform.system() == "Windows" else "sox"
        waveform_paths = self.data_dir / self.metadata["file_name"]
        segment_metadata = []
        for file_i, path in tqdm(enumerate(waveform_paths), desc=f"Saving audio segments to {str(segment_dir)}/ ...."):
            waveform_filename = path.stem
            waveform, sr = torchaudio.load(path)
            audio_len = waveform.size(-1) / sr
            frac_remainder, num_segments = math.modf(audio_len / self.segment_len)
            num_segments = int(num_segments)
            num_samples = int(num_segments * ((self.segment_len * sr) - 1))
            waveform_segments = waveform[:, :num_samples].chunk(chunks=num_segments, dim=-1)
            for seg_i, segment in enumerate(waveform_segments):
                file_path = segment_dir / f"{waveform_filename}_{seg_i}.wav"
                torchaudio.save(uri=str(file_path), src=segment, sample_rate=sr, backend=backend)
                segment_metadata.append((file_i, seg_i, waveform_filename, str(file_path)))
        segment_metadata = pd.DataFrame(segment_metadata, columns=["file_i", "timestep", "segment_file_name", "file_path"])
        segment_metadata.to_parquet(str(segment_dir / "metadata.parquet"), index=True, engine="auto")
        self.logger.info(f"Loading pre-processed audio segments from {str(segment_dir)}")
        return segment_metadata, segment_dir

    def _default_sampler(self, file_path: str) -> Tensor:
        """resample sample audio using specified sample rate and load a segment"""
        metadata = torchaudio.info(str(file_path))
        backend = "soundfile" if platform.system() == "Windows" else "sox"
        num_frames_segment = int(self.num_frames_in_segment / self.sample_rate * metadata.sample_rate)
        high = max(1, metadata.num_frames - num_frames_segment)
        frame_offset = torch.randint(low=0, high=high, size=(1,))
        waveform, _ = torchaudio.load(str(file_path), num_frames=num_frames_segment, backend=backend)
        return torchaudio.functional.resample(waveform, orig_freq=metadata.sample_rate, new_freq=self.sample_rate).squeeze()

    def _extract_coordinates(self):
        """extract recorder co-ordinate information from kml files"""
        coordinates = {}
        for map_file in self._MAP_FILES:
            document = kml.parse(open(self.maps_dir / map_file, "r"))
            for site in document.getroot().Document.Folder.Placemark:
                site_name = str(site.name).upper()
                coordinates[site_name] = {}
                coords = str(site.Point.coordinates).split(',')
                for metric, coord in zip(['longitude', 'latitude', 'altitude'], coords):
                    coordinates[site_name][metric] = float(coord)
        return coordinates

    def __extract_time_metadata(self, row):
        """extract time metadata from file name"""
        _, _, _, year, month, day, hours, minutes = self._FILE_REGEX.match(row.file_name).groups()
        tzinfo = timezone(self._COUNTRY_TO_TZ[row.country])
        timestamp = tzinfo.localize(datetime(int(year), int(month), int(day), int(hours), int(minutes)))
        info = LocationInfo(row.location, row.country, timestamp, row.latitude, row.longitude)
        return dict(
            timestamp=timestamp,
            hour=int(hours),
            time_at_sunrise=sunrise(info.observer, date=timestamp.date(), tzinfo=timestamp.tzinfo),
            time_at_sunset=sunset(info.observer, date=timestamp.date(), tzinfo=timestamp.tzinfo),
            time_at_astronomical_dawn=try_or(lambda: dawn(info.observer, depression=Depression.ASTRONOMICAL, date=timestamp.date(), tzinfo=timestamp.tzinfo)),
            time_at_nautical_dawn=try_or(lambda: dawn(info.observer, depression=Depression.NAUTICAL, date=timestamp.date(), tzinfo=timestamp.tzinfo)),
            time_at_civil_dawn=try_or(lambda: dawn(info.observer, depression=Depression.CIVIL, date=timestamp.date(), tzinfo=timestamp.tzinfo)),
            time_at_astronomical_dusk=try_or(lambda: dusk(info.observer, depression=Depression.ASTRONOMICAL, date=timestamp.date(), tzinfo=timestamp.tzinfo)),
            time_at_nautical_dusk=try_or(lambda: dusk(info.observer, depression=Depression.NAUTICAL, date=timestamp.date(), tzinfo=timestamp.tzinfo)),
            time_at_civil_dusk=try_or(lambda: dusk(info.observer, depression=Depression.CIVIL, date=timestamp.date(), tzinfo=timestamp.tzinfo)),
        )
