import os
import numpy as np
import pandas as pd
import platform
import math
import re
import shutil
import torch
import torchaudio

from conduit.data.datasets.audio.base import CdtAudioDataset
from conduit.data.datasets.utils import AudioTform, UrlFileInfo, download_from_url
from conduit.data.structures import TernarySample
from datetime import datetime
from pandas.api.types import is_categorical_dtype, is_object_dtype
from pandas import DataFrame, Index
from pathlib import Path
from torch import Tensor
from tqdm import tqdm
from typing import (
    Callable,
    ClassVar,
    Dict,
    Final,
    List,
    Optional,
    Tuple,
    Union,
)
from typing_extensions import TypeAlias

__all__ = ["SomersetWildlands"]

SampleType: TypeAlias = TernarySample

def try_or(fn, default=None):
    try:
        return fn()
    except:
        return default

class SomersetWildlands(CdtAudioDataset[SampleType, Tensor, Tensor]):
    """Dataset for audio data collected in various geographic locations."""
    _METADATA_FILENAME: ClassVar[str] = "metadata.parquet"
    _DATA_DIR: ClassVar[str] = "data"

    _FILE_REGEX = re.compile('^(\d+)_(\d+).WAV$')

    num_frames_in_segment: int
    _MAX_AUDIO_LEN: Final[int] = 60
    _BITS_PER_BYTE: int = 8
    _AUDIO_SAMPLE_RATE: int = 48_000
    _BIT_RATE: int = 16

    def __init__(
        self,
        root: str,
        *,
        target_attrs: Optional[List[str]] = None,
        sampler: Optional[object] = None,
        transform: Optional[AudioTform] = None,
        segment_len: float = 60,
        sample_rate: int = 48_000,
        reset_index: bool = False,
    ) -> None:
        self.base_dir = Path(root).expanduser()
        self.data_dir = self.base_dir / self._DATA_DIR
        self.sample_rate = sample_rate
        self.segment_len = segment_len
        self.sampler = sampler
        # extract labels from indices files if first time
        if reset_index or not (self.base_dir / self._METADATA_FILENAME).exists():
            self._extract_metadata()
        # load metadata file
        self.metadata = pd.read_parquet(self.base_dir / self._METADATA_FILENAME)
        self.metadata.index.name = 'file_i'
        # x specifies the files to be loaded by parent dataset class
        x = self.metadata["file_path"].to_numpy()
        # only pass y if available
        if target_attrs is not None:
            y = torch.as_tensor(self._label_encode(self.metadata[target_attrs], inplace=True).to_numpy())
        else:
            y = None
        # add file index as additional tensor
        s = torch.as_tensor(self.metadata.index)
        super().__init__(x=x, y=y, s=s, transform=transform, audio_dir=self.data_dir)


    def load_sample(self, index: int) -> Tensor:
        return self.sampler(self.audio_dir / self.x[index])

    @property
    def segment_len(self) -> float:
        return self._segment_len

    @segment_len.setter
    def segment_len(self, value: float) -> None:
        if value <= 0:
            raise ValueError("Segment length must be positive.")
        value = min(value, self._MAX_AUDIO_LEN)
        self._segment_len = value
        self.num_frames_in_segment = int(self.segment_len * self.sample_rate)

    @property
    def sampler(self) -> Callable:
        return self._sampler

    @sampler.setter
    def sampler(self, sampler: Callable) -> None:
        self._sampler = self._default_sampler if sampler is None else sampler

    @staticmethod
    def _label_encode(
        data: Union[pd.DataFrame, pd.Series],
        inplace=True
    ) -> Union[pd.DataFrame, pd.Series]:
        """label encode the extracted concept/context/superclass information."""
        data = data.copy(deep=not inplace)
        if isinstance(data, pd.Series):
            if is_object_dtype(data) or is_categorical_dtype(data):
                data.update(data.factorize()[0])
                data = data.astype(np.int64)
        else:
            for col in data.columns:
                # add a new column containing the label-encoded data
                if is_object_dtype(data[col]) or is_categorical_dtype(data[col]):
                    data[col] = data[col].factorize()[0]
        return data

    def _extract_metadata(self) -> None:
        """unpack metadata and merge into a single master parquet file"""
        data = []
        subdir_regex = re.compile('^([A-Za-z]+)_([S0-9]+)_(\d+)$')
        for site in ['altheney', 'godney_east', 'godney_waste', 'whites_drove']:
            for directory in os.listdir(self.data_dir / site):
                site_name, recorder_no, date = subdir_regex.search(directory).groups()
                for file_name in os.listdir(self.data_dir / site / directory):
                    timestamp = file_name[:15]
                    file_path = self.data_dir / site / directory / file_name
                    timestamp = datetime.strptime(timestamp, '%Y%m%d_%H%M%S')
                    data.append([str(file_path), file_name, site, site_name + recorder_no, recorder_no, timestamp])
        metadata = pd.DataFrame(data=data, columns=['file_path', 'file_name', 'site', 'site_recorder', 'recorder', 'timestamp'])
        metadata.to_parquet(self.base_dir / self._METADATA_FILENAME, engine="auto", index=True)
        self.logger.info("...metadata extracted and indexed")

    def _default_sampler(self, file_path: str) -> Tensor:
        """resample sample audio using specified sample rate and load a segment"""
        # get metadata first
        metadata = torchaudio.info(file_path)
        # compute number of frames to take with the real sample rate
        num_frames_segment = int(
            self.num_frames_in_segment / self.sample_rate * metadata.sample_rate
        )
        # get random sub-sample
        high = max(1, metadata.num_frames - num_frames_segment)
        frame_offset = torch.randint(low=0, high=high, size=(1,))
        # load segment
        backend = "soundfile" if platform.system() == "Windows" else "sox"
        waveform, _ = torchaudio.load(file_path, num_frames=num_frames_segment, frame_offset=frame_offset, backend=backend)
        # resample to correct sample rate
        return torchaudio.functional.resample(
            waveform,
            orig_freq=metadata.sample_rate,
            new_freq=self.sample_rate,
        )
