import pytest
from sounding_out_torch.data_modules import (
    SoundingOutChorusDataModule,
    SoundingOutChorusVAEEmbeddingsDataModule,
)

def test_sounding_out_chorus_data_module():
    dm = SoundingOutChorusDataModule(
        root="~/data/sounding_out_chorus",
        sample_rate=48_000,
        segment_len=60,
        train_batch_size=6,
    )
    dm.setup()
    batch = next(iter(dm.train_dataloader()))
    assert batch.x.shape == (6, 48_000 * 60)

def test_sounding_out_chorus_vae_embeddings_data_module():
    dm = SoundingOutChorusVAEEmbeddingsDataModule(
        root="~/data/sounding_out_chorus_vae_embeddings",
        latent_dim=128,
        num_samples_per_sequence=16,
        train_batch_size=512,
    )
    dm.setup()
    batch = next(iter(dm.train_dataloader()))
    assert batch.x.shape == (512, 16, 19, 128)
