import pytest
from sounding_out_torch.datasets import (
    SoundingOutChorus,
    SoundingOutChorusVAEEmbeddings,
)

def test_sounding_out_chorus_data_module():
    data = SoundingOutChorus(
        root="~/data/sounding_out_chorus",
        sample_rate=48_000,
        segment_len=60,
        reset_index=True,
    )
    assert data[0].x.shape == (1, 48_000 * 60)

def test_sounding_out_chorus_vae_embeddings_data_module():
    data = SoundingOutChorusVAEEmbeddings(
        root="~/data/sounding_out_chorus_vae_embeddings",
        latent_dim=128,
        num_samples_per_sequence=32,
    )
    assert data[0].x.shape == (32, 19, 128)
