# Ecolistening Torch

A data module for PyTorch to load a variety of ecoacoustic datasets

**Published**:
`SoundingOutChorus` dataset (DOI: 10.5281/zenodo.8338003) available at [https://zenodo.org/deposit/8338003](https://zenodo.org/deposit/8338003).

**Unpublished**:
`SoundingOutDiurnal` dataset
`SoundingOutVAEEmbeddings` dataset
`SomersetWildlands` dataset

## Usage

To load just the dataset:

```python
from sounding_out_torch_data_module import SoundingOut

data = SoundingOutChorus(
    root="/path/to/storage",
    download=True,
    sample_rate=48_000,
    segment_len=60,
    target_attrs=['acoustic_idx', 'bird_ids', 'bird_presence'],
)

```

For using the data module optimised for deep learning:

```python
data_module = SoundingOutChorusDataModule(
    training_mode="step",
    train_batch_size=6,
    eval_batch_size=6,
    val_prop=0.2,
    test_prop=0.2,
    num_workers=48,
    seed=42,
    persist_workers=False,
    pin_memory=True,
    stratified_sampling=False,
    instance_weighting=False,
    root="/path/to/storage",
    segment_len=60,
    sample_rate=48_000,
    target_attrs=['acoustic_idx', 'bird_ids', 'bird_presence'],
    train_transforms=[],
    test_transforms=[],
)
```

A set of transforms can be passed using a pytorch `Module` which implements a `forward` method, for example using a `Sequential` or `torchvision.transforms.Compose`.
